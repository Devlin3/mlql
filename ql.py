import numpy as np
import random
import time
import curses
import traceback
import copy
np.set_printoptions(threshold=np.inf)

NORMAL = 0
NEG = 1
NEG_WALL = 2
#possible moves that can be made
P = 4
ACTIONS_NUM = [0,1,2,3,4]
ACTIONS_DELTAS = [(1, 0), (-1,0), (0,1), (0, -1), (0,0)]
ACTION_MAP = {k : v for k, v in zip(ACTIONS_NUM, ACTIONS_DELTAS)}
ACTIONS_NAMES = ["S", "N", "E", "W", "P"] 

#possilbe square values
EMPTY = 0
WALL = 1
CAN = 2
LOC_NUM = [EMPTY, WALL, CAN]
LOC_NAME = ["E", "W", "C"]

class QLRobby:
    """
        Perfroms QL learning in in a nXm grid world.
        Params:
            lr - learning rate (double) [0,1]
            discount - discount factor aka gamma (double) [0,1]
            numR - number of rows in grid world (int) (1,inf)
            numC - number of columns in grid owrld (int) (1,inf)
            screen - nCurses screen. used when simulation is done is visual mode
            sparsity - percent chance a square contains a can (double) [0,1]
    """
    def __init__(self, lr, discount, numR, numC, screen, sparsity = .5):
        self.lr = lr
        self.discount = discount
        self.numR = numR
        self.numC = numC
        self.sparsity = sparsity
        self.board = None
        self.agentLoc = None
        self.qTable = np.zeros([len(LOC_NAME)**len(ACTIONS_NUM), len(ACTIONS_NUM)])
        self.accumRewards = np.array([])
        self.screen = screen

    #clears out reward table for new simulation
    def resetRewards(self):
        self.accumRewards = np.array([])
        
    #randomly places cans and agent to start new episode
    def initBoard(self):
        #board with all empty cells
        board = np.zeros(shape=(self.numR, self.numC), dtype=np.int8)
        
        #create walls on perimeter
        board[:,0] = WALL
        board[:,-1] = WALL
        board[0] = WALL
        board[-1] = WALL

        #randomly place cans in middle
        for i in range(1,self.numR - 1):
            for j in range(1, self.numC - 1):
                if random.random() < self.sparsity:
                    board[i][j] = CAN
        
        #randomly place agent in middle
        loc = (random.randint(1, self.numR - 2), random.randint(1, self.numC - 2))

        self.board = board
        self.agentLoc = loc

    #assings value for an action in a given state
    def reward(self, action, default = 0):
        nR, nC = self.newLoc(action)
        if self.board[nR][nC] == WALL:
            return -5
        if action == P and self.board[nR][nC] == CAN:
            return 10.0
        if action == P:
            return - 1.0
        
        return default

    #adjuts location of agent based on action
    def newLoc(self, action):
        rDelta, cDelta = ACTION_MAP[action]
        nR = self.agentLoc[0] + rDelta
        nC = self.agentLoc[1] + cDelta
        return nR, nC

    #applys action and updates the board
    def takeAction(self, action):
        nR, nC = self.newLoc(action)
        loc = self.board[nR][nC]
        
        #ran into wall
        if loc == WALL:
            return
        
        #picked up can
        if loc == CAN and action == P:
            self.board[nR][nC] = EMPTY
            return
        
        #moved to a new tile
        self.agentLoc = (nR, nC)

    #convert sensor input to the q row
    def stateEnc(self, state):
        numItems = len(LOC_NAME)
        row = 0
        for i in range(len(state)):
            row += state[i] * (numItems ** i)
        return int(row)

    #check neighbor and current square
    def getState(self):
        r, c  = self.agentLoc
        return[self.board[r + rD][c + cD] for (rD, cD) in  ACTIONS_DELTAS]
    
    #decides the enct action to take
    def pickAction(self, ep, index):
        if random.random() < ep:
            return random.randint(0, len(ACTIONS_NUM) - 1)

        #pick action with highest Q, breaking ties at random.
        #rounding floats to two digits to stop running into corners
        table = np.around(self.qTable[index], decimals=1)
        top = np.argwhere(table == np.amax(table))
        return random.choice(top.flatten().tolist())

    #prints current board configuration to screen
    def updateUI(self, curReward, pause, action):
        for i in range(self.numR):
            for j in range(self.numC):
             try:
                self.screen.addch(i,j, LOC_NAME[self.board[i][j]])
             except curses.error:
                pass
        try:
            self.screen.addch(self.agentLoc[0], self.agentLoc[1], "P")
        except curses.error:
            pass
        try:
            self.screen.addstr(self.numR,0, "Last episode reward: " + str(ql.accumRewards[-1]))
        except:
            pass
        
        state = self.getState()
        origStateIndex = self.stateEnc(state)
        self.screen.addstr(self.numR + 1, 0, str(ACTIONS_NAMES))
        self.screen.addstr(self.numR + 2, 0, str(self.qTable[origStateIndex]))
        self.screen.addstr(self.numR + 4, 0, "Action: " + str(ACTIONS_NAMES[action]))
        self.screen.addstr(self.numR + 5, 0, "Current reward: " + str(curReward))
        
        self.screen.refresh()

        if pause:
            self.screen.getch()

    #runs episode and stores the latest reward
    def simEpisode(self, updateUI, updateQ, pause, ep = .1,maxMoves = 200):
        self.initBoard()
        cumReward = 0
        for i in range(maxMoves):

            #take action and recieve reward
            state = self.getState()
            origStateIndex = self.stateEnc(state)
            action = self.pickAction(ep, origStateIndex)
            reward = self.reward(action)
            cumReward += reward 
            
            if updateUI:
                self.updateUI(cumReward, pause,action)

            self.takeAction(action)

            if updateQ:
                #generate new state
                newState = self.getState()
                newStateIndex = self.stateEnc(newState)

                #update Q table
                curQ = self.qTable[origStateIndex][action]
                futureQ = np.amax(self.qTable[newStateIndex])
                
                newQ = curQ + self.lr * (reward + (self.discount * futureQ) - curQ)
                self.qTable[origStateIndex][action] = newQ
         
        self.accumRewards = np.append(self.accumRewards, [cumReward])

#applys negative reward for any action taken
class QLRobbyNeg(QLRobby):
    def reward(self, action, default = -.5):
        nR, nC = self.newLoc(action)
        if self.board[nR][nC] == WALL:
            return -5.5
        if action == P and self.board[nR][nC] == CAN:
            return 9.5
        if action == P:
            return - 1.5
        return default

#applys negative reward for being next to a wall
class QLRobbyJD(QLRobby):
    def reward(self, action, default = 0):
        nR, nC = self.newLoc(action)
        if self.board[nR][nC] == WALL:
            return -5.0
        elif  action == P and self.board[nR][nC] == CAN:
            return  10.0
        if action == P:
            return - 1.0
        
        #punish for being near walls
        for rd, cd in ACTION_MAP.values():
            r = rd + nR
            c = cd + nC
            if self.board[r][c] == WALL:
                default -= .1
        return default

#prints data required for report
def printResult(train, test):
    print(np.mean(train.reshape(-1, 100), axis=1))
    print(np.mean(test.reshape(-1, 100), axis=1))
    
    print("Training Mean: " + str(np.mean(train)))
    print("Testing Mean: " + str(np.mean(test)))

    print("Training STD: " + str(np.std(train)))
    print("Testing STD: " + str(np.std(test)))

def runExp(episodes, lr, ep, gamma, type, screen = None, update = False, pause = False, rows = 11, cols = 11):
    ql = None
    if type == NORMAL:
        ql = QLRobby(lr,  gamma, rows, cols, screen)
    elif type == NEG:
        ql = QLRobbyNeg(lr,  gamma, rows, cols, screen)
    elif type == NEG_WALL:
        ql = QLRobbyJD(lr,  gamma, rows, cols, screen)
    else:
        print("error deciding which class to use")

    ql.resetRewards()
    trainingRewards = None
    testRewards = None
    #staic EP for tarining
    if ep:
        for i in range(episodes):
            ql.simEpisode(update, True, pause, ep)
            
    #scaling ep for training
    else:
        ep = 1
        for i in range(episodes):
            if i % 50 == 0 and ep > .1:
                ep -= .01
            ql.simEpisode(update, True, pause, ep)
    trainingRewards = copy.deepcopy(ql.accumRewards)
        
    ql.resetRewards()    
    for i in range(episodes):
        ql.simEpisode(update, False, pause)
    
    testRewards = copy.deepcopy(ql.accumRewards)
    printResult(trainingRewards, testRewards)

if __name__ == "__main__":
    print("exp1")
    runExp(5000, .9, None, .2, NORMAL)

    #change learning rate
    print("Modifying LR")
    for lr in [.2, .4, .6, .8]:
        print (lr)
        runExp(5000, lr, None, .2, NORMAL)

    print("Modify Epsilon")
    for ep in [.2, .4, .6, .8]:
        print (ep)
        runExp(5000, .9, ep, .2, NORMAL)

    print("Modify rewards neg")
    runExp(5000, .9, None, .2, NEG)
    
    print("Modify state rewards wall")
    runExp(5000, .9, None, .2, NEG_WALL)
